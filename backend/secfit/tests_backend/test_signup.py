import json
import unittest
from rest_framework.test import APIRequestFactory, APIClient


class TestSignupBoundaryValue(unittest.TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.user = json.dumps({
            'username': 'TomRiddle',
            'password': 'Harry',
            'phone_number': '01458290351',
            'country': 'England',
            'city': 'Hogwarts',
            'street_address': 'something_magic'
        })

    def testStuff(self):
        factory = self.factory
        request = factory.post('/api/users/', self.user, content_type='application/json')

        client = APIClient()
        client.post('/api/users/', self.user, format='json')


    def tearDown(self):
        pass

# Using the standard RequestFactory API to create a form POST request
