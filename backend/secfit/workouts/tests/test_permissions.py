from django.test import TestCase
from rest_framework import permissions
from django.utils import timezone

from ..permissions import *
from users.models import User
from workouts.models import Workout

def User_Data(name):
    u_data = {
            "username":name,
            "coach":None,
            "phone_number":45677234,
            "country": "Polen",
            "city": "Polenbyen",
            "street_address": "Monopolgata 78"
    }
    return u_data

# Create your tests here.
class Request_Data():
    def __init__(self):
        u_data = {
            "username":"HannePettersen",
            "coach":None,
            "phone_number":90088666,
            "country": "Gløslandet",
            "city": "The street",
            "street_address": "Streetveien 16"
        }
        self.method = None
        self.data = ""
        self.user = User.objects.create(**User_Data("Ole Andersen"))


class Workout_Data():
    def __init__(self):
        self.user = User.objects.create()    
        w_data = {
            "name": "The best workout",
            "date": timezone.now(),
            "notes": "Workout is the best",
            "owner": self.user,
            "visibility": "PU"
        }
        u_data = {
            "username":"HannePettersen",
            "coach":None,
            "phone_number":90088666,
            "country": "Gløslandet",
            "city": "The street",
            "street_address": "Streetveien 16"
        }
        self.workout = Workout.objects.create(**w_data)     
        self.workout.owner.coach = User.objects.create(**u_data)

class TestIsOwner(TestCase):#OK
    def setUp(self):
        self.is_owner = IsOwner()
        self.request = Request_Data()
        self.obj = Workout_Data()

    def test_has_object_permission(self):
        self.assertFalse(self.is_owner.has_object_permission(self.request, None, self.obj.workout))
        self.request.user = self.obj.workout.owner   
        self.assertTrue(self.is_owner.has_object_permission(self.request, None, self.obj.workout))
        

class TestIsOwnerOfWorkout(TestCase): #OK
    def setUp(self):
        self.is_owner_of_workout = IsOwnerOfWorkout()
        self.request = Request_Data()
        self.obj = Workout_Data()
        self.request.method = "POST"

    def test_has_permissionTTT(self): # true, true, true
        self.request.user = self.obj.workout.owner
        self.request.data = {"workout": "http://web:80/api/workouts/1/"}
        self.assertTrue(self.is_owner_of_workout.has_permission(self.request, None))

    def test_has_permissionTTF1(self): # true, true, false
        self.request.user = User.objects.create(**User_Data("Sara Lien"))
        self.request.data = {"workout": "http://web:80/api/workouts/1/"}
        self.assertFalse(self.is_owner_of_workout.has_permission(self.request, None))

    def test_has_permissionTF(self): # true, false
        self.request.data = {"workout": ""}
        self.assertFalse(self.is_owner_of_workout.has_permission(self.request, None))

    def test_has_permissionF(self): #false
        self.request.method = "GET"
        self.assertTrue(self.is_owner_of_workout.has_permission(self.request, None))
    
    def test_has_object_permission(self):#OK
        self.assertFalse(self.is_owner_of_workout.has_object_permission(self.request, None, self.obj))
        self.request.user = self.obj.workout.owner   
        self.assertTrue(self.is_owner_of_workout.has_object_permission(self.request, None, self.obj))
       

class TestIsCoachAndVisibleToCoach(TestCase):#ok
    def setUp(self):
        self.is_coach_and_visible_to_coach = IsCoachAndVisibleToCoach()
        self.request = Request_Data()
        self.obj = Workout_Data()
        
    def test_has_object_permission(self):
        self.assertFalse(self.is_coach_and_visible_to_coach.has_object_permission(self.request, None, self.obj.workout))
        self.obj.workout.owner.coach = self.request.user
        self.assertTrue(self.is_coach_and_visible_to_coach.has_object_permission(self.request, None, self.obj.workout))
    

class TestIsCoachOfWorkoutAndVisibleToCoach(TestCase):#ok
    def setUp(self):
        self.is_coach_of_workout_and_visible_to_coach = IsCoachOfWorkoutAndVisibleToCoach()
        self.request = Request_Data()
        self.obj = Workout_Data()


    def test_has_object_permission(self):
        self.assertFalse(self.is_coach_of_workout_and_visible_to_coach.has_object_permission(self.request, None, self.obj))
        self.obj.workout.owner.coach = self.request.user
        self.assertTrue(self.is_coach_of_workout_and_visible_to_coach.has_object_permission(self.request, None, self.obj))
        
class TestIsPublic(TestCase):
    def setUp(self):
        self.is_public = IsPublic()
        self.obj = Workout_Data()

    def test_has_object_permission(self):
        self.assertTrue(self.is_public.has_object_permission(None, None, self.obj.workout))
        self.obj.workout.visibility = "PR"
        self.assertFalse(self.is_public.has_object_permission(None, None, self.obj.workout))

class TestIsWorkoutPublic(TestCase):
    def setUp(self):
        self.is_workout_public = IsWorkoutPublic()
        self.obj = Workout_Data()

    def test_has_object_permission(self):
        self.assertTrue(self.is_workout_public.has_object_permission(None, None, self.obj))
        self.obj.workout.visibility = "PR"
        self.assertFalse(self.is_workout_public.has_object_permission(None, None, self.obj))

class TestIsReadOnly(TestCase):
    def setUp(self):
        self.is_read_only = IsReadOnly()
        self.request = Request_Data()

    def test_has_object_permission(self):
        self.assertFalse(self.is_read_only.has_object_permission(self.request, None, None))
        self.request.method = permissions.SAFE_METHODS.__getitem__(1)
        self.assertTrue(self.is_read_only.has_object_permission(self.request, None, None))
