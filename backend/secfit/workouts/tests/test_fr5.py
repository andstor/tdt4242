import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import datetime
import time


# python manage.py test workouts.fr5test.TestViewWorkouts
def User_Login(self, username):
    self.driver.get("http://web:80/login.html")
    self.wait.until(presence_of_element_located((By.ID, "btn-login")))
    login = self.driver.find_element_by_id('btn-login')
    name = self.driver.find_element_by_name('username')
    password = self.driver.find_element_by_name('password')
    rememberme = self.driver.find_element_by_id('rememberMe')
    name.send_keys(username)
    password.send_keys("pass123")
    rememberme.click()
    login.click()
    

def User_Logout(self):
    logout = self.wait.until(element_to_be_clickable((By.ID, 'btn-logout')))
    logout.click()

def User_Registrer(self, username):
    self.driver.get("http://web:80/register.html")
    usernameField = self.driver.find_element_by_name('username')
    emailField = self.driver.find_element_by_name('email')
    passwordField = self.driver.find_element_by_name('password')
    repeatPasswordField = self.driver.find_element_by_name('password1')
    phoneNumberField = self.driver.find_element_by_name('phone_number')
    countryField = self.driver.find_element_by_name('country')
    cityField = self.driver.find_element_by_name('city')
    streetAddressField = self.driver.find_element_by_name('street_address')

    usernameField.send_keys(username)
    emailField.send_keys(username+"@noe.no")
    passwordField.send_keys("pass123")
    repeatPasswordField.send_keys("pass123")
    phoneNumberField.send_keys("45677654")
    countryField.send_keys("Norge")
    cityField.send_keys("Trondheim")
    streetAddressField.send_keys("olav tryggvasons gate 3")
    self.wait.until(presence_of_element_located((By.ID, "btn-create-account")))
    self.driver.find_element_by_id("btn-create-account").click()
    time.sleep(0.5)


def Add_Workout(self, visibility):
    newExercise = self.driver.find_element_by_id("nav-exercises")
    newExercise.click()
    time.sleep(0.5)
    self.wait.until(presence_of_element_located((By.ID, "btn-create-exercise")))
    self.driver.find_element_by_id("btn-create-exercise").click()
    newExerciseName = self.driver.find_element_by_name("name")
    newExerciseDescription = self.driver.find_element_by_name('description')
    newExerciseUnit = self.driver.find_element_by_name('unit')
        
    newExerciseName.send_keys("Situps")
    newExerciseDescription.send_keys("Take as many situps as yuo can")
    newExerciseUnit.send_keys("s")
    self.wait.until(presence_of_element_located((By.ID, "btn-ok-exercise"))) 
    self.driver.find_element_by_id("btn-ok-exercise").click()
        
    #------------------------------------------------------
    time.sleep(0.5)
    self.driver.find_element_by_id("nav-workouts").click()

    newWorkoutButton = self.driver.find_element_by_id("btn-create-workout")
    newWorkoutButton.click()
    workoutNameField = self.driver.find_element_by_id("inputName")
    workoutDateField = self.driver.find_element_by_id("inputDateTime")
    workoutNotesField = self.driver.find_element_by_id("inputNotes")
    workoutVisibilityField = self.driver.find_element_by_id("inputVisibility")
    workoutTypeField = self.driver.find_element_by_name("type")
    workoutSetsField = self.driver.find_element_by_name("sets")
    workoutNumberField = self.driver.find_element_by_name("number")

    workoutNameField.send_keys("The best Workout")
    workoutDateField.send_keys("20-04-002021T20:30")
    workoutNotesField.send_keys("The coolest workout")
    workoutVisibilityField.send_keys(visibility)
    workoutTypeField.send_keys("Situps")
    workoutSetsField.send_keys("1")
    workoutNumberField.send_keys("1")
    
    scroll_to_buttom(self)
    self.wait.until(presence_of_element_located((By.ID, "btn-add-exercise")))
    btnexercise = self.driver.find_element_by_id("btn-add-exercise")
    btnexercise.click()
    
    self.wait.until(presence_of_element_located((By.ID, "btn-ok-workout")))
    self.driver.find_element_by_id("btn-ok-workout").click()
    time.sleep(0.5)
   
def scroll_to_buttom(self):
    html = self.driver.find_element_by_tag_name('html')
    html.send_keys(Keys.END)


class TestViewWorkouts(unittest.TestCase):
    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("window-size=1600x900")
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.implicitly_wait(30)
        self.wait = WebDriverWait(self.driver, 10)

    def test_access_public_workouts(self):
        User_Registrer(self,"HannePettersen")
        Add_Workout(self,"PU")
        workoutElement = self.driver.find_elements_by_css_selector("a.list-group-item")[-1]
        workoutElement.click()
        workoutOwner = self.driver.find_element_by_name("owner_username").get_attribute("value")
        workoutVisibility = self.driver.find_element_by_name("visibility").get_attribute("value")
        
        self.assertEqual(workoutOwner,"HannePettersen")
        self.assertEqual(workoutVisibility,"PU")
    
        User_Logout(self)    
        User_Registrer(self,"NinaAndersen")
        workoutElement = self.driver.find_elements_by_css_selector("a.list-group-item")[-1]
        workoutElement.click()
        workoutOwner = self.driver.find_element_by_name("owner_username").get_attribute("value")
        workoutVisibility = self.driver.find_element_by_name("visibility").get_attribute("value")

        self.assertEqual(workoutOwner,"HannePettersen")
        self.assertEqual(workoutVisibility,"PU")


    def test_access_public_see_comments(self):
        User_Registrer(self, "DagLund")
        Add_Workout(self,"PU")
        workoutElement = self.driver.find_elements_by_css_selector("a.list-group-item")[-1]
        workoutElement.click()
        scroll_to_buttom(self)
       
        comment = self.wait.until(presence_of_element_located((By.ID, 'comment-area')))
        time.sleep(1)
        comment.send_keys("Denne workouten er aa anbefale!")
        scroll_to_buttom(self)
        time.sleep(1)
        btn_comment = self.wait.until(presence_of_element_located((By.ID, "post-comment")))
        btn_comment.click()
        scroll_to_buttom(self)
        
        commented_coment=None
        try:
            commented_coment=self.driver.find_element_by_id("comment-text").text
        except:
            print("comment error")
        self.assertTrue(commented_coment == "Denne workouten er aa anbefale!")
        
        self.driver.get("http://web:80/workouts.html")
        User_Logout(self)
        User_Registrer(self,"PernilleHageland")
        workoutElement = self.driver.find_elements_by_css_selector("a.list-group-item")[-1]
        workoutElement.click()
        scroll_to_buttom(self)
        commented_coment=None
        try:
            commented_coment=self.driver.find_element_by_id("comment-text").text
        except:
            print("comment error")
        self.assertTrue(commented_coment == "Denne workouten er aa anbefale!")
    
    def test_access_private_workouts(self):
        User_Registrer(self,"NoraHansen")
        Add_Workout(self,"PR")
        myWorkoutsButton = self.driver.find_element_by_id("list-my-workouts-list")
        myWorkoutsButton.click()
        workoutElement = self.driver.find_elements_by_css_selector("a.list-group-item")[-1]
        workoutElement.click()
        workoutOwner = self.driver.find_element_by_name("owner_username").get_attribute("value")
        workoutVisibility = self.driver.find_element_by_name("visibility").get_attribute("value")

        self.assertEqual(workoutOwner,"NoraHansen")
        self.assertEqual(workoutVisibility,"PR")

        User_Logout(self)
        User_Registrer(self,"NinaAndersen")
        aWorkout = None
        try:
            aWorkout = self.driver.find_elements_by_css_selector("a.list-group-item")[-1]
        except:
            print("An exception occurred")
        self.assertTrue(aWorkout != None)
     

    def test_access_coach_workouts_by_coach(self):
        User_Registrer(self,"EmmaNilsen")        
        User_Logout(self)
        User_Registrer(self, "EspenLund")
        self.driver.get("http://web:80/myathletes.html")
        self.wait.until(presence_of_element_located((By.ID, "button-submit-roster")))
        athlete = self.driver.find_element_by_name("athlete")
        btn_Submitt = self.driver.find_element_by_id("button-submit-roster")
        athlete.send_keys("EmmaNilsen")
        btn_Submitt.click()
        time.sleep(0.5)
        User_Logout(self)
        User_Login(self,"EmmaNilsen")
        time.sleep(0.5)
        self.driver.get("http://web:80/mycoach.html")
        btn_accept = self.driver.find_elements_by_css_selector("button.btn.btn-success")[-1]
        btn_accept.click()
        time.sleep(0.5)
        self.driver.get("http://web:80/workouts.html")
        Add_Workout(self,"CO")
        User_Logout(self)
        User_Login(self,"EspenLund")
        athlete_Workout = self.driver.find_elements_by_css_selector("a.list-group-item")[-1]
        athlete_Workout.click()
        self.wait.until(presence_of_element_located((By.NAME, "owner_username")))
        self.wait.until(presence_of_element_located((By.NAME, "visibility")))
        workoutOwner = self.driver.find_element_by_name("owner_username").get_attribute("value")
        workoutVisibility = self.driver.find_element_by_name("visibility").get_attribute("value")
        self.assertEqual(workoutOwner,"EmmaNilsen")
        self.assertEqual(workoutVisibility,"CO")
        


        