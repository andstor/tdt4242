import unittest
from enum import Enum

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable


def fillTextElement(inputFieldElement, text):
    #inputFieldElement.click()
    inputFieldElement.clear()
    #inputFieldElement.click()
    while inputFieldElement.get_attribute('value') == '':
        inputFieldElement.send_keys(str(text))
    inputFieldElement.send_keys(Keys.TAB)


def navigateSignupPage(self):
    registerButton = self.wait.until(element_to_be_clickable((By.ID, 'btn-register')))
    registerButton.click()

    self.createAccountButton = self.wait.until(presence_of_element_located((By.ID, 'btn-create-account')))

    self.userNameInput = self.wait.until(presence_of_element_located((By.NAME, 'username')))
    self.userPasswordInput = self.wait.until(presence_of_element_located((By.NAME, 'password')))
    self.userPasswordConfirmInput = self.wait.until(presence_of_element_located((By.NAME, 'password1')))

    self.emailInput = self.wait.until(presence_of_element_located((By.NAME, 'email')))
    self.phoneNumberInput = self.wait.until(presence_of_element_located((By.NAME, 'phone_number')))
    self.countryInput = self.wait.until(presence_of_element_located((By.NAME, 'country')))
    self.cityInput = self.wait.until(presence_of_element_located((By.NAME, 'city')))
    self.streetAddressInput = self.wait.until(presence_of_element_located((By.NAME, 'street_address')))

def navigateWorkoutPage(self):
    workoutsButton = self.wait.until(element_to_be_clickable((By.ID, 'nav-workouts')))
    workoutsButton.click()

    logNewWorkoutButton = self.wait.until(element_to_be_clickable((By.ID, 'btn-create-workout')))
    logNewWorkoutButton.click()

    self.createWorkoutButton = self.wait.until(element_to_be_clickable((By.ID, 'btn-ok-workout')))
    self.addExercisesButton = self.wait.until(element_to_be_clickable((By.ID, 'btn-add-exercise')))

    self.workoutNameInput = self.wait.until(presence_of_element_located((By.ID, 'inputName')))
    self.workoutDateTimeInput = self.wait.until(presence_of_element_located((By.ID, 'inputDateTime')))
    self.workoutNotesInput = self.wait.until(presence_of_element_located((By.ID, 'inputNotes')))

    self.workoutExercisesSetsInput = self.wait.until(presence_of_element_located((By.NAME, 'sets')))
    self.workoutExercisesNumberInput = self.wait.until(presence_of_element_located((By.NAME, 'number')))

    self.visibilityInput = self.wait.until(presence_of_element_located((By.ID, 'inputVisibility')))
    self.exercisesType = self.wait.until(presence_of_element_located((By.NAME, 'type')))

def logoutUser(self):
    login_nav_button = self.wait.until(element_to_be_clickable((By.ID, 'btn-logout')))
    login_nav_button.click()

    self.wait.until(presence_of_element_located((By.ID, "btn-login-nav")))


class DashboardImageClassNames(Enum):
    latestPersonalWorkout = '.latest-personal-workout-img'
    personalWorkout = '.personal-workout-img'
    recentWorkout = '.new-workout-img'


class DashboardTextClassNames(Enum):
    latestPersonalWorkout = '.latest-personal-workout-text'
    personalWorkout = '.personal-workout-text'
    recentWorkout = '.new-workout-text'
