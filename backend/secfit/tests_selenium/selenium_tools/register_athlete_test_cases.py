import time
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable, \
    url_contains, visibility_of_any_elements_located

from users.models import User

from tests_selenium.selenium_tools.general_athlete_test_cases import AthleteUserTestCase

from tests_selenium.selenium_tools import selenium_helper
from tests_selenium.selenium_tools.selenium_helper import fillTextElement
from tests_selenium.tests_boundary_value.TestSet import TestSet
from selenium.webdriver.chrome.options import Options


class SeleniumUserTestCase(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("window-size=1200x600")
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=chrome_options)
        self.server_url = 'http://web:80/'
        self.driver.get(self.server_url)
        self.wait = WebDriverWait(self.driver, 10)

    def tearDown(self):
        self.driver.close()
        self.driver.quit()


class RegisterUserTestCase(SeleniumUserTestCase):
    userNameInput = None
    userPasswordInput = None
    userPasswordConfirmInput = None

    emailInput = None
    phoneNumberInput = None
    countryInput = None
    cityInput = None
    streetAddressInput = None

    def setUp(self):
        super().setUp()

        self.user: User = None


    def tearDown(self):
        if self.user is not None:
            self.user.delete()

        super().tearDown()


class CreateWorkoutTestCase(AthleteUserTestCase):
    workoutNameInput = None
    workoutDateTimeInput = None
    workoutNotesInput = None

    workoutExercisesSetsInput = None
    workoutExercisesNumberInput = None

    def setUp(self):
        super().setUp()

        self.loginUser(self.driver)

    def fillTestFormData(self, shouldBeSuccessful, testSpecifiers):
        testDict = self._getTestDictData(testSpecifiers)

        for i in range(0, len(max(testDict.values(),key=len))):
            testSample = self._getTestSample(i, testDict)
            self._createWorkout(testSample)

            if shouldBeSuccessful: self._validateSuccessfulWorkoutCreation()
            else: self._validateUnsuccessfulWorkoutCreation()

    def _createWorkout(self, testSetDict):
        selenium_helper.navigateWorkoutPage(self)

        fillTextElement(self.workoutNameInput, testSetDict.get('name'))
        fillTextElement(self.workoutNotesInput, testSetDict.get('notes'))

        dateTimeArray = testSetDict.get('dateTime').rsplit('/')
        self.assertTrue(len(dateTimeArray), 5)
        self._fillDateTimeField(*dateTimeArray)

        fillTextElement(self.workoutExercisesSetsInput, testSetDict.get('exercisesSets'))
        fillTextElement(self.workoutExercisesNumberInput, testSetDict.get('exercisesNumber'))
        self.driver.execute_script("arguments[0].click();", self.addExercisesButton)

        self.createWorkoutButton.click()

    def _fillDateTimeField(self, day='', month='', year='', hour='', minute=''):
        self.workoutDateTimeInput.click()

        time.sleep(0.5)

        self.workoutDateTimeInput.send_keys(str(day))
        self.workoutDateTimeInput.send_keys(str(month))
        self.workoutDateTimeInput.send_keys(str(year))

        if len(year) < 5:
            self.workoutDateTimeInput.send_keys(Keys.TAB)

        self.workoutDateTimeInput.send_keys(str(hour))
        self.workoutDateTimeInput.send_keys(str(minute))

    def _getTestDictData(self, testSpecifiers):
        testDict = dict()
        testFieldDeclarers = list(testSpecifiers.keys())
        testFieldCaseTypes = list(testSpecifiers.values())

        for i in range(0, len(testSpecifiers)):
            testCaseSpecifiedDict = TestSet.createWorkoutSetDict.get(testFieldCaseTypes[i])
            testDict.update({testFieldDeclarers[i]: testCaseSpecifiedDict.get(testFieldDeclarers[i])})

        return testDict

    def _getTestSample(self, i, testDict):
        testSample = {
            "name": testDict.get("name")[i] if i < len(testDict.get("name")) else testDict.get("name")[
                -1],
            "dateTime": testDict.get("dateTime")[i] if i < len(testDict.get("dateTime")) else testDict.get("dateTime")[
                -1],
            "notes": testDict.get("notes")[i] if i < len(testDict.get("notes")) else testDict.get("notes")[-1],
            "exercisesSets": testDict.get("exercisesSets")[i] if i < len(testDict.get("exercisesSets")) else
            testDict.get("exercisesSets")[-1],
            "exercisesNumber": testDict.get("exercisesNumber")[i] if i < len(testDict.get("exercisesNumber")) else
            testDict.get("exercisesNumber")[-1],
            "visibilityInput": testDict.get("visibilityInput")[i] if i < len(testDict.get("visibilityInput")) else
            testDict.get("visibilityInput")[-1],
            "exercisesType": testDict.get("exercisesType")[i] if i < len(testDict.get("exercisesType")) else
            testDict.get("exercisesType")[-1],
        }

        return testSample

    def _validateSuccessfulWorkoutCreation(self):
        assert not len(self.driver.find_elements_by_css_selector(".alert.alert-warning.alert-dismissible"))
        self.wait.until_not(url_contains('workout.html'))

    def _validateUnsuccessfulWorkoutCreation(self):
        self.wait.until(visibility_of_any_elements_located(
            (By.CSS_SELECTOR, ".alert.alert-warning.alert-dismissible")))
        self.wait.until(url_contains('workout.html'))
