import unittest
import importlib

from django.test import Client
from django.conf import settings
from django.utils.datetime_safe import datetime

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By

from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable
from selenium.webdriver.chrome.options import Options

from users.models import User
from workouts.models import Workout, WorkoutFile, Exercise, ExerciseInstance

from tests_selenium.selenium_tools import selenium_helper


# Test case will just connect to page
class AthleteUserTestCase(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("window-size=1200x600")
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=chrome_options)

        self.server_url = 'http://web:80/'
        self.driver.get(self.server_url)
        self.wait = WebDriverWait(self.driver, 10)

        self.user_password_clear = '1234'
        self.user = User.objects.create_user('RomTiddle', 'havenot@hogwarts.uk', self.user_password_clear, phone_number='0123467812638')
        self.user.save()

    def loginUser(self, driver):
        login_nav_button = driver.find_element_by_id('btn-login-nav')
        login_nav_button.click()

        self.wait.until(presence_of_element_located((By.ID, "btn-login")))

        login_name = self.wait.until(element_to_be_clickable((By.NAME, "username")))
        self.wait.until(presence_of_element_located((By.NAME, "username")))
        login_password = self.wait.until(element_to_be_clickable((By.NAME, "password")))

        selenium_helper.fillTextElement(login_name, self.user.username)
        selenium_helper.fillTextElement(login_password, self.user_password_clear)

        login_button = driver.find_element_by_id('btn-login')
        login_button.click()

    def tearDown(self):
        self.user.delete()

        self.driver.close()
        self.driver.quit()


    def validateDashboardElement(self):
        dashboard_container = self.wait.until(presence_of_element_located((By.ID, "dashboard-container")))
        dashboard_title = dashboard_container.find_element_by_css_selector(
            'tbody>tr>td>div>div>div>div>div>div>table>tbody>tr>td>h1')
        self.assertIn(dashboard_title.text, 'Dashboard')

    # persisting test data to the db


# Test case will login in on top and will create dummy workout data for testing purposes
class AthleteDummyDataTestCase(AthleteUserTestCase):
    def setUp(self):
        super().setUp()

        self.setUpWorkoutDummys()

        self.latestPersonalExercise.save()
        self.latestPersonalWorkout.save()
        self.latestPersonalWorkoutFile.save()
        self.latestWorkoutExerciseInstance.save()

        self.personalWorkout.save()
        self.personalWorkoutFile.save()
        self.personalWorkoutExerciseInstance.save()

        self.foreignRecentWorkout.save()
        self.foreignRecentWorkoutFile.save()
        self.foreignRecentWorkoutExerciseInstance.save()

        self.loginUser(self.driver)

    def setUpWorkoutDummys(self):
        #latestPersonalWorkout
        self.latestPersonalWorkout = Workout.objects.create(name='LatestPersonalWorkout',
                                                            date=datetime(2020, 12, 11, 12, 11, 0).astimezone(),
                                                            notes='this is a test note', owner=self.user,
                                                            visibility=Workout.PUBLIC)

        self.latestPersonalExercise = Exercise.objects.create(name='Basic Exercise',
                                                              description='not here',
                                                              unit='reps')

        self.latestWorkoutExerciseInstance = ExerciseInstance.objects.create(workout=self.latestPersonalWorkout,
                                                                             exercise=self.latestPersonalExercise,
                                                                             sets=2,
                                                                             number=35)

        self.latestPersonalWorkoutFile = WorkoutFile.objects.create(workout=self.latestPersonalWorkout, owner=self.user,
                                                                    file='workouts/0/Man-Doing-Crunches-At-Home.jpg')

        #personalWorkout
        self.personalWorkout = Workout.objects.create(name='PersonalWorkout',
                                                      date=datetime(1997, 12, 11, 12, 11, 0).astimezone(),
                                                      notes='this is an older note', owner=self.user,
                                                      visibility=Workout.PUBLIC)

        self.personalWorkoutExerciseInstance = ExerciseInstance.objects.create(workout=self.personalWorkout,
                                                                               exercise=self.latestPersonalExercise,
                                                                               sets=1,
                                                                               number=2)

        self.personalWorkoutFile = WorkoutFile.objects.create(workout=self.personalWorkout, owner=self.user,
                                                              file='workouts/0/Man-Doing-Crunches-At-Home.jpg')

        #foreignRecentWorkout
        adminUser = User.objects.get(username='admin')
        self.foreignRecentWorkout = Workout.objects.create(name='ForeignRecentWorkout',
                                                           date=datetime.now().astimezone(),
                                                           notes='this is an foreign recent note', owner=adminUser,
                                                           visibility=Workout.PUBLIC)

        self.foreignRecentWorkoutExerciseInstance = ExerciseInstance.objects.create(workout=self.foreignRecentWorkout,
                                                                                    exercise=self.latestPersonalExercise,
                                                                                    sets=1,
                                                                                    number=2)

        self.foreignRecentWorkoutFile = WorkoutFile.objects.create(workout=self.foreignRecentWorkout, owner=adminUser,
                                                                   file='workouts/0/Man-Doing-Crunches-At-Home.jpg')

    def tearDown(self):
        super().tearDown()

        self.latestPersonalExercise.delete()
        self.latestPersonalWorkout.delete()
        self.latestPersonalWorkoutFile.delete()
        self.latestWorkoutExerciseInstance.delete()

        self.personalWorkout.delete()
        self.personalWorkoutExerciseInstance.delete()
        self.personalWorkoutFile.delete()

        self.foreignRecentWorkout.delete()
        self.foreignRecentWorkoutExerciseInstance.delete()
        self.foreignRecentWorkoutFile.delete()