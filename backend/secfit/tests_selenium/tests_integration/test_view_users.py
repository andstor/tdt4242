import time
import unittest

from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable
from tests_selenium.selenium_tools import general_athlete_test_cases

from users.models import User


class ValidateUserListPage(general_athlete_test_cases.AthleteUserTestCase):
    def setUp(self):
        super().setUp()

        self.otherTestUser = User.objects.create_user('Ronny', 'otherUser@stuff.no', "otherPW", phone_number="045289135723")
        self.otherTestUser.save()

        self.loginUser(self.driver)

    def testAllUsersDisplayed(self):
        driver = self.driver

        self.validateDashboardElement()

        navbar_users = driver.find_element_by_xpath('//*[@id="nav-users"]')
        driver.execute_script("arguments[0].click();", navbar_users)
        profileListContainerDiv = self.wait.until(presence_of_element_located((By.CLASS_NAME, 'list-group')))
        allUserInList = profileListContainerDiv.find_elements_by_class_name('list-group-item')

        otherUserFound: bool = False
        for userElement in allUserInList:
            userTextElement = userElement.find_element_by_xpath('.//h5')
            if userTextElement.text == self.otherTestUser.username:
                otherUserFound = True

        self.assertTrue(otherUserFound)

    def testVisitOtherUserProfile(self):
        driver = self.driver

        self.validateDashboardElement()

        navbar_users = driver.find_element_by_xpath('//*[@id="nav-users"]')
        driver.execute_script("arguments[0].click();", navbar_users)
        profileListContainerDiv = self.wait.until(presence_of_element_located((By.CLASS_NAME, 'list-group')))

        otherUser = self.otherTestUser
        otherUserTextElement = profileListContainerDiv.find_element_by_xpath('.//h5[text() = "'+otherUser.username+'"]/../..')
        driver.execute_script("arguments[0].click();", otherUserTextElement)

        email_input = self.wait.until(element_to_be_clickable((By.ID, 'inputEmail')))
        email_input.click()
        self.assertIn(email_input.get_attribute('value'), otherUser.email)

    def tearDown(self):
        super().tearDown()

        if self.otherTestUser is not None:
            self.otherTestUser.delete()


if __name__ == "__main__":
    unittest.main()
