import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class SeleniumTestClass(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("window-size=1200x600")
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(options=chrome_options)

        self.server_url = 'http://web:80/'

    def testLoginPage(self):
        driver = self.driver
        driver.get(self.server_url)
        wait = WebDriverWait(driver, 10)

        self.assertIn("Home", driver.title)
        login_button = driver.find_element_by_id('btn-login-nav')
        login_button.click()

        self.assertIn('Login', driver.title)
        wait.until(presence_of_element_located((By.ID, "btn-login")))
        login_confirmation = driver.find_element_by_id('btn-login')
        self.assertIn(login_confirmation.text, 'Log In')

        login_name = driver.find_element_by_name('username')
        login_password = driver.find_element_by_name('password')
        login_name.clear()
        login_password.clear()

    def tearDown(self):
        self.driver.close()
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()