import time
import unittest

from django.utils.datetime_safe import datetime
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable

from tests_selenium.selenium_tools import general_athlete_test_cases
from workouts.models import Workout, WorkoutFile, Exercise, ExerciseInstance
from users.models import User
from tests_selenium.selenium_tools.selenium_helper import DashboardImageClassNames, DashboardTextClassNames


class ValidateDashboardClass(general_athlete_test_cases.AthleteUserTestCase):
    def setUp(self):
        super().setUp()

    # Testcase 1
    def testDashboardPresence(self):
        super().loginUser(self.driver)

        self.validateDashboardElement()

    # Testcase 2
    def testNavigationToDashboard(self):
        driver = self.driver
        super().loginUser(self.driver)

        self.validateDashboardElement()

        navbar_exercises = driver.find_element_by_xpath('//*[@id="nav-exercises"]')
        driver.execute_script("arguments[0].click();", navbar_exercises)
        self.wait.until(presence_of_element_located((By.ID, 'btn-create-exercise')))

        navbar_home = driver.find_element_by_xpath('//*[@id="nav-index"]')
        driver.execute_script("arguments[0].click();", navbar_home)

        self.validateDashboardElement()

    def testDummyImagesDisplayed(self):
        driver = self.driver

        dummy_images = driver.find_elements_by_css_selector('.preview-image.dummy-image')
        self.assertEqual(len(dummy_images), 8)

        for image in dummy_images:
            src_url = image.get_attribute('src')
            self.assertTrue(src_url.endswith('.jpg') or src_url.endswith('.png'))


class ValidateDashboardDataClass(general_athlete_test_cases.AthleteDummyDataTestCase):
    def testLatestPersonalData(self):
        driver = self.driver

        self.validateDashboardElement()

        imageContainersInRow = driver.find_elements_by_css_selector(DashboardImageClassNames.latestPersonalWorkout.value)
        workoutObjects = self._extractWorkoutData(imageContainersInRow)
        self._checkForOwnership(workoutObjects)
        self._checkForRecentOrder(workoutObjects)

    def testPersonalData(self):
        driver = self.driver

        self.validateDashboardElement()

        imageContainersInRow = driver.find_elements_by_css_selector(DashboardImageClassNames.personalWorkout.value)
        workoutObjects = self._extractWorkoutData(imageContainersInRow)
        self._checkForOwnership(workoutObjects)

    def testRecentData(self):
        driver = self.driver

        self.validateDashboardElement()

        imageContainersInRow = driver.find_elements_by_css_selector(DashboardImageClassNames.recentWorkout.value)
        workoutObjects = self._extractWorkoutData(imageContainersInRow)
        self._checkForRecentOrder(workoutObjects)

    def _extractWorkoutData(self, imageContainersInRow):
        workoutObjects = []

        for imageContainer in imageContainersInRow:
            try:
                workoutHref = imageContainer.find_element_by_xpath('.//a').get_attribute('href')
            except NoSuchElementException:
                break

            workout = Workout.objects.get(id=workoutHref.rsplit("id=")[1])
            workoutObjects.append(workout)

        return workoutObjects

    def _checkForRecentOrder(self, workoutObjects):
        for i in range(0, len(workoutObjects) - 1):
            self.assertTrue(workoutObjects[i].date > workoutObjects[i + 1].date)

    def _checkForOwnership(self, workoutObjects):
        for workout in workoutObjects:
            self.assertTrue(workout.owner.username == self.user.username)


class ValidateDashboardNavigationClass(general_athlete_test_cases.AthleteDummyDataTestCase):
    def testLatestPersonalWorkouts(self):
        self.validateDashboardElement()

        driver = self.driver
        firstWorkoutContainer = driver.find_element_by_css_selector(DashboardImageClassNames.latestPersonalWorkout.value + '.column-1')
        image = firstWorkoutContainer.find_element_by_class_name('preview-image')
        driver.execute_script("arguments[0].click();", image)

        self._validateWorkoutChoice(self.latestPersonalWorkout)

    def testPersonalWorkouts(self):
        self.validateDashboardElement()

        driver = self.driver

        personalWorkoutContainers = driver.find_elements_by_css_selector(DashboardTextClassNames.personalWorkout.value)

        for textContainer in personalWorkoutContainers:
            if textContainer.get_attribute('value') == self.personalWorkout.name:
                textContainerColumnClassIndex = textContainer.get_attribute('class').rsplit(' column-')[1]

                firstWorkoutImageContainers = driver.find_element_by_css_selector(
                    DashboardImageClassNames.personalWorkout.value + '.column-' + textContainerColumnClassIndex)

                image = firstWorkoutImageContainers.find_element_by_class_name('preview-image')
                self.assertIsNotNone(image)
                driver.execute_script("arguments[0].click();", image)

                self._validateWorkoutChoice(self.personalWorkout)

    def testRecentWorkouts(self):
        self.validateDashboardElement()

        driver = self.driver
        firstWorkoutContainer = driver.find_element_by_css_selector(DashboardImageClassNames.recentWorkout.value + '.column-1')
        image = firstWorkoutContainer.find_element_by_class_name('preview-image')
        driver.execute_script("arguments[0].click();", image)

        self._validateWorkoutChoice(self.foreignRecentWorkout)

    def _validateWorkoutChoice(self, workout):
        workoutNameField = self.wait.until(presence_of_element_located((By.ID, 'inputName')))
        self.assertIn(workoutNameField.get_attribute('value'), workout.name)


if __name__ == "__main__":
    unittest.main()
