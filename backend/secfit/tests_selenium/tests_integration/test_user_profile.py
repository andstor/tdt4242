import time
import unittest

from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import presence_of_element_located, element_to_be_clickable, \
    alert_is_present

from tests_selenium.selenium_tools import general_athlete_test_cases

from django.core.exceptions import ObjectDoesNotExist

from users.models import User

from tests_selenium.selenium_tools import selenium_helper


class ValidateUserProfilePage(general_athlete_test_cases.AthleteUserTestCase):
    def setUp(self):
        super().setUp()
        self.loginUser(self.driver)

    def testUserProfileDisplayed(self):
        driver = self.driver

        self.validateDashboardElement()

        navbar_profile = driver.find_element_by_xpath('//*[@id="nav-profile"]')
        driver.execute_script("arguments[0].click();", navbar_profile)
        profileInformationDiv = self.wait.until(presence_of_element_located((By.ID, 'profileinformation')))

        displayedUsername = profileInformationDiv.find_element_by_xpath('.//h6')
        self.assertIn(displayedUsername.text, self.user.username)

    def testUserProfileInfoChanged(self):
        driver = self.driver

        self.validateDashboardElement()

        navbar_profile = driver.find_element_by_xpath('//*[@id="nav-profile"]')
        driver.execute_script("arguments[0].click();", navbar_profile)

        edit_user_btn = self.wait.until(element_to_be_clickable((By.ID, "btn-edit-user")))
        edit_user_btn.click()

        new_mail = 'haveAMail@hogwarts.uk'
        email_input = driver.find_element_by_id('inputEmail')
        selenium_helper.fillTextElement(email_input, new_mail)

        info_change_btn = driver.find_element_by_id('btn-ok-user-edit')
        info_change_btn.click()

        self.assertEqual(email_input.get_attribute('value'), new_mail)

    def testDeleteUserProfileAbort(self):
        driver = self.driver

        self.validateDashboardElement()

        navbar_profile = driver.find_element_by_xpath('//*[@id="nav-profile"]')
        driver.execute_script("arguments[0].click();", navbar_profile)

        edit_user_btn = self.wait.until(element_to_be_clickable((By.ID, "btn-delete-user")))
        edit_user_btn.click()

        self.wait.until(alert_is_present())
        alert = driver.switch_to.alert
        alert.dismiss()

    def testDeleteUserProfileAccept(self):
        driver = self.driver

        self.validateDashboardElement()

        navbar_profile = driver.find_element_by_xpath('//*[@id="nav-profile"]')
        driver.execute_script("arguments[0].click();", navbar_profile)

        edit_user_btn = self.wait.until(element_to_be_clickable((By.ID, "btn-delete-user")))
        edit_user_btn.click()

        self.wait.until(alert_is_present())
        alert = driver.switch_to.alert
        alert.accept()

        deletedUser: User = None
        try:
            # not a beautiful check - assertion check in catch could need better version
            self.wait.until(presence_of_element_located((By.ID, "welcome-text")))
            deletedUser = User.objects.get(id=self.user.id)
        except ObjectDoesNotExist:
            self.assertEqual(deletedUser, None)

if __name__ == "__main__":
    unittest.main()
