

class TestSet:
    signUpSetDict = {
        "name": {
            "minMinus": [" ", "€%", "!?", ":;=", "^^"],
            "min": ["a", "A", "abc", "a1", "0", "0a", "äöü", "øå"],
            "minPlus": ["bbbbbb", "BBBBB", "bcdefghijklmnopqrstuvwxy"],
            "nom": ["TestAvatar", "TestAvatar234", "TestAvatar_123"],
            "maxMinus": ["y", "Y", "wyx", "y9", "Y9", "Y1234567890",
                         "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdef"],
            "max": ["z", "Z", "abc", "a1",
                    "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefg",
                    "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"],
            "maxPlus": ["ø«~~«@", "Ω∑®",
                        "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxy"],
        },
        "password": {
            "minMinus": [" "],
            "min": ["a", "A", "abc", "a1", "0", "0a", "äöü", "øå"],
            "minPlus": ["bbbbbb", "BBBBB", "bcdefghijklmnopqrstuvwxy"],
            "nom": ["TestCharacter", "TestCharacter234", "TestCharacter_123"],
            "maxMinus": ["y", "Y", "wyx", "y9", "Y9", "Y1234567890",
                         "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdef"],
            "max": ["z", "Z", "abc", "a1",
                    "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghi",
                    "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"],
            "maxPlus": [
                "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxy"],
        },
        "email": {
            "minMinus": [" "],
            "min": ["a@a.a"],
            "minPlus": ["bb@bb.bb"],
            "nom": ["test@gmail.com"],
            "maxMinus": ["bcdefghijklmnopqrstuvwxybcde@fghijklmnopqrstu.vwx"],
            "max": ["bcdefghijklmnopqrstuvwxybcdefgh@ijklmnopqrstuv.wxy"],
            "maxPlus": [
                "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxbcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghi@jklmnopqrstuvwxybcdefghijklmn.opq"],
        },
        "phoneNumber":{
            "minMinus": [" "],
            "min": ["a"],
            "minPlus": ["bb"],
            "nom": ["016279177421"],
            "maxMinus": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxyb"],
            "max": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybc"],
            "maxPlus": [
                "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcd"],
        },
        "country":{
            "minMinus": [" "],
            "min": ["a"],
            "minPlus": ["bb"],
            "nom": ["Norway"],
            "maxMinus": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxyb"],
            "max": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybc"],
            "maxPlus": [
                "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcd"],

        },
        "city": {
            "minMinus": [" "],
            "min": ["a"],
            "minPlus": ["bb"],
            "nom": ["Berlin"],
            "maxMinus": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxyb"],
            "max": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybc"],
            "maxPlus": [
                "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcd"],
        },
        "streetAddress": {
            "minMinus": [" "],
            "min": ["a"],
            "minPlus": ["bb"],
            "nom": ["Nostreet 12"],
            "maxMinus": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxyb"],
            "max": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybc"],
            "maxPlus": [
                "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcd"],
        }
    }

    createWorkoutSetDict = {
        "nom": {
            "name": ["TestWorkout"],
            "dateTime": ["12/11/2020/12/01"],
            "notes": ["test Notes"],
            "exercisesSets": ["5"],
            "exercisesNumber": ["20"],
            "visibilityInput": ["Public"],
            "exercisesType": ["Crunch"],
        },

        "minMinus": {
            "name": [" "],
            "dateTime": [""],
            "notes": [" "],
            "exercisesSets": ["-2", "1.23", "-1.23", "-9223372036854775808"],
            "exercisesNumber": ["-1000", "2.3692487673891", "-123.12", "-9223372036854775808"],
            "visibilityInput": [""],
            "exercisesType": [""],
        },
        "min": {
            "name": [".", "a", "A", "1", "a1", "1a"],
            "dateTime": ["00/00/0000/00/00"],
            "notes": [".", "a", "A", "1", "a1", "1a"],
            "exercisesSets": ["0"],
            "exercisesNumber": ["0"],
            "visibilityInput": [""],
            "exercisesType": ["Push-up"],
        },
        "minPlus": {
            "name": ["bbbbbb", "BBBBB", "bcdefghijklmnopqrstuvwxy"],
            "dateTime": ["01/01/0001/01/01"],
            "notes": ["bbbbbb", "BBBBB", "bcdefghijklmnopqrstuvwxy"],
            "exercisesSets": ["1"],
            "exercisesNumber": ["1"],
            "visibilityInput": [""],
            "exercisesType": ["Push-up"],
        },
        "maxMinus": {
            "name": ["y", "Y", "wyx", "y9", "Y9", "Y1234567890", "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"],
            "dateTime": ["30/11/9998/23/58"],
            "notes": ["y", "Y", "wyx", "y9", "Y9", "Y1234567890", "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"],
            "exercisesSets": ["9223372036854775806"],
            "exercisesNumber": ["9223372036854775806"],
            "visibilityInput": ["Coach"],
            "exercisesType": [""],
        },
        "max": {
            "name": ["zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"],
            "dateTime": ["31/12/9999/23/59"],
            "notes": ["zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"],
            "exercisesSets": ["9223372036854775807"],
            "exercisesNumber": ["9223372036854775807"],
            "visibilityInput": ["Private"],
            "exercisesType": ["Plank"],
        },
        "maxPlus": {
            "name": ["zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz", "bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxy"],
            "dateTime": ["31/02/1999/25/60"],
            "notes": ["bcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxybcdefghijklmnopqrstuvwxy"],
            "exercisesSets": ["92233720368547758089223372036854775808"],
            "exercisesNumber": ["92233720368547758089223372036854775808"],
            "visibilityInput": [""],
            "exercisesType": [""],
        },
    }
