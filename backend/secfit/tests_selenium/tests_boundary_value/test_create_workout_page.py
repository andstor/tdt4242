import time
import unittest

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.expected_conditions import url_contains

from tests_selenium.selenium_tools import register_athlete_test_cases
from tests_selenium.selenium_tools import selenium_helper
from tests_selenium.selenium_tools.selenium_helper import fillTextElement
from tests_selenium.tests_boundary_value.TestSet import TestSet


class ValidateCreateWorkoutBoundaryValues(register_athlete_test_cases.CreateWorkoutTestCase):
    def testNameNom(self):
        self.fillTestFormData(True, dict(name='nom', dateTime='min', notes='minPlus', exercisesSets='maxMinus', exercisesNumber='max', visibilityInput='min', exercisesType='max'))

    def testDateTimeNom(self):
        self.fillTestFormData(True, dict(name='max', dateTime='nom', notes='min', exercisesSets='minPlus', exercisesNumber='maxMinus', visibilityInput='max', exercisesType='min'))

    def testNotesNom(self):
        self.fillTestFormData(True, dict(name='min', dateTime='max', notes='nom', exercisesSets='min', exercisesNumber='minPlus', visibilityInput='maxMinus', exercisesType='nom'))

    def testExercisesSetsNom(self):
        self.fillTestFormData(True, dict(name='minPlus', dateTime='maxMinus', notes='max', exercisesSets='nom', exercisesNumber='min', visibilityInput='minPlus', exercisesType='maxMinus'))

    def testExercisesNumberNom(self):
        self.fillTestFormData(True, dict(name='maxMinus', dateTime='minPlus', notes='maxMinus', exercisesSets='max', exercisesNumber='nom', visibilityInput='nom', exercisesType='minPlus'))

#test max amount of exercises

class ValidateCreateWorkoutOutOfBoundaryValues(register_athlete_test_cases.CreateWorkoutTestCase):
    def testNameNomDateTimeMaxPlus(self):
        self.fillTestFormData(False, dict(name='nom', dateTime='maxPlus', notes='min', exercisesSets='max', exercisesNumber='maxMinus', visibilityInput='minPlus', exercisesType='maxPlus'))

    def testNameNomDateTimeMinMinus(self):
        self.fillTestFormData(False, dict(name='nom', dateTime='minMinus', notes='min', exercisesSets='max', exercisesNumber='maxMinus', visibilityInput='minPlus', exercisesType='maxPlus'))

    def testDateTimeNomNotesMaxPlus(self):
        self.fillTestFormData(False, dict(name='maxMinus', dateTime='nom', notes='maxPlus', exercisesSets='min', exercisesNumber='max', visibilityInput='minPlus', exercisesType='maxPlus'))

    def testDateTimeNomNotesMinMinus(self):
        self.fillTestFormData(False, dict(name='maxMinus', dateTime='nom', notes='minMinus', exercisesSets='min', exercisesNumber='max', visibilityInput='minPlus', exercisesType='maxPlus'))

    def testNotesNomExercisesSetsMaxPlus(self):
        self.fillTestFormData(False, dict(name='max', dateTime='maxMinus', notes='nom', exercisesSets='maxPlus', exercisesNumber='min', visibilityInput='maxPlus', exercisesType='minMinus'))

    def testNotesNomExercisesSetsMinMinus(self):
        self.fillTestFormData(False, dict(name='max', dateTime='maxMinus', notes='nom', exercisesSets='minMinus', exercisesNumber='min', visibilityInput='maxPlus', exercisesType='minMinus'))

    def testExercisesSetsNomExercisesNumberMaxPlus(self):
        self.fillTestFormData(False, dict(name='min', dateTime='max', notes='maxMinus', exercisesSets='nom', exercisesNumber='maxPlus', visibilityInput='maxPlus', exercisesType='minMinus'))

    def testExercisesSetsNomExercisesNumberMinMinus(self):
        self.fillTestFormData(False, dict(name='min', dateTime='max', notes='maxMinus', exercisesSets='nom', exercisesNumber='minMinus', visibilityInput='maxPlus', exercisesType='minMinus'))

    def testExercisesNumberNomNameMaxPlus(self):
        self.fillTestFormData(False, dict(name='maxPlus', dateTime='min', notes='max', exercisesSets='maxMinus', exercisesNumber='nom', visibilityInput='maxPlus', exercisesType='minMinus'))

    def testExercisesNumberNomNameMinMinus(self):
        self.fillTestFormData(False, dict(name='minMinus', dateTime='min', notes='max', exercisesSets='maxMinus', exercisesNumber='nom', visibilityInput='maxPlus', exercisesType='minMinus'))


if __name__ == "__main__":
    unittest.main()