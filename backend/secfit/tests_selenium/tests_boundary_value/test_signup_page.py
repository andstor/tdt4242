import time
import unittest

from django.core.exceptions import ObjectDoesNotExist
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import visibility_of_any_elements_located, url_contains, \
    presence_of_element_located, staleness_of, visibility_of_element_located, invisibility_of_element, \
    invisibility_of_element_located
from tests_selenium.selenium_tools import register_athlete_test_cases, selenium_helper
from tests_selenium.selenium_tools.selenium_helper import fillTextElement
from tests_selenium.tests_boundary_value.TestSet import TestSet
from users.models import User

"""
Boundary Value Tests with selenium for signup page
X - Name
Y - Password

Nom - normal value
Min - Minimum value on boundary
MinPlus - Minimum value next to boundary
Max - Maximum value on boundary
MaxMinus - Maximum value next to boundary
"""


# validate passing
class ValidateSignupValidBoundaryValues(register_athlete_test_cases.RegisterUserTestCase):
    def _registerWithAllEntries(self, nameStringArray, passwordStringArray):
        for i in range(0, len(nameStringArray)):
            for j in range(0, len(passwordStringArray)):
                selenium_helper.navigateSignupPage(self)

                fillTextElement(self.userNameInput, nameStringArray[i])
                fillTextElement(self.userPasswordInput, passwordStringArray[j])
                fillTextElement(self.userPasswordConfirmInput, passwordStringArray[j])

                self.createAccountButton.click()
                try:
                    assert not len(self.driver.find_elements_by_css_selector(".alert.alert-warning.alert-dismissible"))
                    self.wait.until(invisibility_of_element_located((By.ID, 'btn-create-account')))

                    selenium_helper.logoutUser(self)
                except TimeoutException:
                    pass
                finally:
                    try:
                        time.sleep(0.5)
                        self._deleteCreatedUser(nameStringArray[i])
                    except ObjectDoesNotExist:
                        pass

    def _deleteCreatedUser(self, username):
        self.user = User.objects.get(username=username)
        self.user.delete()
        self.user = None

    def testXNomYNom(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('nom')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('nom')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXNomYMin(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('nom')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('min')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXNomYMinPlus(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('nom')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('minPlus')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXNomYMaxMinus(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('nom')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('maxMinus')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXNomYMax(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('nom')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('max')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXMinYNom(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('min')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('nom')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXMinPlusYNom(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('minPlus')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('nom')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXMaxMinusYNom(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('maxMinus')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('nom')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXMaxYNom(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('max')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('nom')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)


# validate not passing
class ValidateSignupInvalidBoundaryValues(register_athlete_test_cases.RegisterUserTestCase):
    def _registerWithAllEntries(self, nameStringArray, passwordStringArray):
        for i in range(0, len(nameStringArray)):
            for j in range(0, len(passwordStringArray)):
                selenium_helper.navigateSignupPage(self)

                fillTextElement(self.userNameInput, nameStringArray[i])
                fillTextElement(self.userPasswordInput, passwordStringArray[j])
                fillTextElement(self.userPasswordConfirmInput, passwordStringArray[j])

                self.createAccountButton.click()

                try:
                    self.wait.until(visibility_of_any_elements_located(
                        (By.CSS_SELECTOR, ".alert.alert-warning.alert-dismissible")))
                    self.wait.until(visibility_of_element_located((By.ID, 'btn-create-account')))
                except TimeoutException:
                    print(nameStringArray[i] + ' ' + passwordStringArray[j])

    def testXNomYMinMinus(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('nom')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('minMinus')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXMinMinusYNom(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('minMinus')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('nom')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

    def testXMaxPlusYNom(self):
        nameStringArray = TestSet.signUpSetDict.get('name').get('maxPlus')
        passwordStringArray = TestSet.signUpSetDict.get('password').get('nom')

        self._registerWithAllEntries(nameStringArray, passwordStringArray)

        """ MISSING VALIDATION ON SITE FOUND """

class ValidateSignupUncheckedFieldValues(register_athlete_test_cases.RegisterUserTestCase):
    def _registerWithAllEntries(self, testDictData):
        for i in range(0, len(max(testDictData.values(), key=len))):
            testSample = self._getTestSample(i, testDictData)
            self.signUp(testSample)

            try:
                self.wait.until(visibility_of_any_elements_located(
                    (By.CSS_SELECTOR, ".alert.alert-warning.alert-dismissible")))
                self.wait.until(visibility_of_element_located((By.ID, 'btn-create-account')))
            except TimeoutException:
                print("test set was detected as valid, but it shouldn't: " + testSample)

    def signUp(self, testSample):
        selenium_helper.navigateSignupPage(self)

        fillTextElement(self.userNameInput, testSample.get('name'))
        fillTextElement(self.userPasswordInput, testSample.get('password'))
        fillTextElement(self.userPasswordConfirmInput, testSample.get('password'))

        fillTextElement(self.emailInput, testSample.get('email'))
        fillTextElement(self.phoneNumberInput, testSample.get('phoneNumber'))
        fillTextElement(self.countryInput, testSample.get('country'))
        fillTextElement(self.cityInput, testSample.get('city'))
        fillTextElement(self.streetAddressInput, testSample.get('streetAddress'))

        self.createAccountButton.click()

    def _getTestSample(self, i, testDict):
        testSample = {
            "name": testDict.get("name")[i] if i < len(testDict.get("name")) else testDict.get("name")[
                -1],
            "password": testDict.get("password")[i] if i < len(testDict.get("password")) else testDict.get("password")[
                -1],
            "email": testDict.get("email")[i] if i < len(testDict.get("email")) else testDict.get("email")[-1],
            "phoneNumber": testDict.get("phoneNumber")[i] if i < len(testDict.get("phoneNumber")) else
            testDict.get("phoneNumber")[-1],
            "country": testDict.get("country")[i] if i < len(testDict.get("country")) else
            testDict.get("country")[-1],
            "city": testDict.get("city")[i] if i < len(testDict.get("city")) else
            testDict.get("city")[-1],
            "streetAddress": testDict.get("streetAddress")[i] if i < len(testDict.get("streetAddress")) else
            testDict.get("streetAddress")[-1],
        }

        return testSample

    def _defineTestSet(self, **kwargs):
        testDict = dict()
        testFieldDeclarers = list(kwargs.keys())
        testFieldCaseTypes = list(kwargs.values())

        for i in range(0, len(kwargs)):
            testCaseSpecifiedDict = TestSet.signUpSetDict.get(testFieldDeclarers[i])
            testDict.update({testFieldDeclarers[i]: testCaseSpecifiedDict.get(testFieldCaseTypes[i])})

        return testDict

    def testEmailMaxPlus(self):
        testSet = self._defineTestSet(name='nom', password='nom', email='maxPlus', phoneNumber='minPlus', country='max',
                                      city='min', streetAddress='maxMinus')
        self._registerWithAllEntries(testSet)

    def testPhoneNumberMaxPlus(self):
        testSet = self._defineTestSet(name='nom', password='nom', email='maxMinus', phoneNumber='maxPlus',
                                      country='minPlus', city='max', streetAddress='min')
        self._registerWithAllEntries(testSet)

    def testCountryMaxPlus(self):
        testSet = self._defineTestSet(name='nom', password='nom', email='min', phoneNumber='maxMinus',
                                      country='maxPlus', city='minPlus', streetAddress='max')
        self._registerWithAllEntries(testSet)

    def testCityMaxPlus(self):
        testSet = self._defineTestSet(name='nom', password='nom', email='max', phoneNumber='min', country='maxMinus',
                                      city='maxPlus', streetAddress='minPlus')
        self._registerWithAllEntries(testSet)

    def testStreetAddressMaxPlus(self):
        testSet = self._defineTestSet(name='nom', password='nom', email='minPlus', phoneNumber='max', country='min',
                                      city='maxMinus', streetAddress='maxPlus')
        self._registerWithAllEntries(testSet)


if __name__ == "__main__":
    unittest.main()
