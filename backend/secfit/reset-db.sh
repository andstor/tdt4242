#!/bin/bash
#
echo yes | python manage.py flush
DJANGO_SUPERUSER_USERNAME=admin \
    DJANGO_SUPERUSER_PASSWORD=Password \
    DJANGO_SUPERUSER_EMAIL=admin@mail.com \
    python manage.py createsuperuser --noinput
python manage.py loaddata seed.json