from django.test import TestCase
from rest_framework import serializers
from django import forms


from ..models import User
from ..serializers import UserSerializer


def User_Data(password, password1):
    u_data = {
            "email":"hanne_pettersen@noe.no",
            "username": "HannePettersen",
            "password": password,
            "password1": password1,
            "phone_number": 45677654,
            "country":"Norge",
            "city":"Trondheim",
            "street_address":"olav tryggvasons gate 3",
    }
    return u_data

class TestUserSerializer(TestCase):
    def setUp(self):
        self.userSerializer = UserSerializer(data=User_Data("pass123", "pass123"))

    def test_validate_password(self):
        response = self.userSerializer.validate_password("pass123")
        self.assertEqual(response, "pass123")

    def test_validate_passwordFail(self):
        userSerializer = UserSerializer(data=User_Data("abc", "abc"))
        with self.assertRaises(serializers.ValidationError): 
           userSerializer.validate_password("abc")

    def test_validate_passwordMissmatch(self):
        userSerializer = UserSerializer(data=User_Data("abcde", "fghij"))
        with self.assertRaises(serializers.ValidationError): 
           userSerializer.validate_password("abc")

    def test_create(self):
        self.u_data = User_Data("pass123", "pass123")
        self.user = self.userSerializer.create(User_Data("pass123", "pass123")) 
        self.assertEqual(self.user.username, self.u_data['username'])
        self.assertEqual(self.user.email, self.u_data['email'])
        self.assertEqual(self.user.phone_number, self.u_data['phone_number'])
        self.assertEqual(self.user.country, self.u_data['country'])
        self.assertEqual(self.user.city, self.u_data['city'])
        self.assertEqual(self.user.street_address, self.u_data['street_address'])