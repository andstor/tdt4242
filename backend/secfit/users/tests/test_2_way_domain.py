
from allpairspy import AllPairs
from django.test import TestCase, Client
parameters = [
            ["HannePettersen", "Hanne Pettersen", ""],
            ["hanne_pettersen@noe.no", "hanne_pettersen.no", ""],
            ["password", None, ""],
            ["password", None, ""],
            ["45677654", "...............................................................", ""],
            ["Norge", "...............................................................", ""],
            ["Trondheim", "...............................................................", ""],
            ["olav tryggvasons gate 3", "...............................................................", ""]
]
class UserRegistration(TestCase):
    def setUp(self):
        self.client = Client()
        self.pair = []
        self.elements_in_pairs=[]
        for i, pairs in enumerate(AllPairs(parameters)):
            data = {
                "username":pairs[0],
                "email":pairs[1],
                "password":pairs[2],
                "password1":pairs[3],
                "phone_number":pairs[4],
                "country":pairs[5],
                "city":pairs[6],
                "street_address":pairs[7],
            }
            self.elements_in_pairs.append([pairs[0],pairs[1],pairs[2],pairs[3],pairs[4],pairs[5],pairs[6],pairs[7]])
            self.pair.append(data)

    def test_two_way_domain(self):
        for i in range (len(self.pair)):
            code = 201
            if (self.elements_in_pairs[i][0] == "" or self.elements_in_pairs[i][0] =="Hanne Pettersen" or
            self.elements_in_pairs[i][1] == "hanne_pettersen.no" or
            self.elements_in_pairs[i][2] == None or self.elements_in_pairs[i][3] == None or
            len(self.elements_in_pairs[i][4]) > 50 or
            len(self.elements_in_pairs[i][5]) > 50 or
            len(self.elements_in_pairs[i][6]) > 50 or
            len(self.elements_in_pairs[i][7]) > 50):
               code = 400
            try:
                response = self.client.post('/api/users/', self.pair[i])
                self.assertEqual(response.status_code, code)
            except:
                self.assertEqual(code, 400)
                

