#!/bin/bash
#
# Bash script to setup headless Selenium (uses Chrome)

# Add Google Chrome's repo to sources.list
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'

# Install tools:
apt-get -y update
apt-get -y install wget unzip curl gnupg

# Install Google's public key used for signing packages (e.g. Chrome)
# (Source: http://www.google.com/linuxrepositories/)
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

# Install Google Chrome:
apt-get -y update
apt-get -y install google-chrome-stable
rm /etc/apt/sources.list.d/google-chrome.list

# Download/Install chromedriver
# (http://code.google.com/p/chromedriver/):
#
wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip

unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/