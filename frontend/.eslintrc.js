module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    //"extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 12
    },
    "rules": {
        "no-undef": "off",
        "indent": [
            "warn",
            4
        ],
        "linebreak-style": [
            "warn",
            "unix"
        ],
        "quotes": [
            "warn",
            "double"
        ],
        "semi": [
            "warn",
            "always"
        ],
        "max-lines-per-function": ["warn", 20],
        "max-classes-per-file": ["error", 1],
        "id-length": [2, { "exceptions": ["i", "j"], "properties": "never" }],
        "max-depth": [1,2]
    }
};
