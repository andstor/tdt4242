let profilepageUtility = StringUtility.ProfilePageUtility;

document.addEventListener("DOMContentLoaded", async () => {
    let currentUser = await getCurrentUser();
    let profilepage = new Profilepage(currentUser);
    profilepage.initialize();
});

class Profilepage{
    constructor(currentUser) {
        this.currentUser = currentUser;
        this.apiRequester = new ApiRequester();
        this.oldMail = "";

        this.deleteButton = document.querySelector(profilepageUtility.deleteButtonId.toElementId());
        this.editButton = document.querySelector(profilepageUtility.editButtonId.toElementId());
        this.cancelButton = document.querySelector(profilepageUtility.cancelEditButtonId.toElementId());
        this.okButton = document.querySelector(profilepageUtility.confirmEditButtonId.toElementId());

        this.inputMail = document.querySelector(profilepageUtility.profileEmailInputId.toElementId());
        this.workoutHeading = document.querySelector(profilepageUtility.workoutHeadingId.toElementId());
    }

    async initialize(){
        let fetchedWorkoutData = await this.#fetchWorkoutData();
        let workouts = fetchedWorkoutData.results;

        this.#initiateWorkoutList(workouts);

        let user = await this.#fetchShownUser();
        this.#retrieveUserProfile(user);
        this.#showProfileWorkouts(workouts, user);
    }

    async #fetchWorkoutData(){
        let requestUrl = this.apiRequester.buildRequestQuery(["workouts"]);
        let fetchedWorkoutData = await this.apiRequester.apiRequest(METHOD.GET, requestUrl,
            profilepageUtility.ErrorMessages.FetchingWorkoutsFailed);

        return fetchedWorkoutData;
    }

    #initiateWorkoutList(workouts){
        let workoutListContainer = document.querySelector(profilepageUtility.workoutContainerId.toElementId());
        let workoutTemplate = document.querySelector(profilepageUtility.workoutTemplateId.toElementId());

        workouts.forEach(workout => {
            let clonedWorkoutTemplate = workoutTemplate.content.cloneNode(true);
            let clickableWorkoutElement = clonedWorkoutTemplate.querySelector(profilepageUtility.clickableTemplateWrapper);
            clickableWorkoutElement.href = StringUtility.workoutHref + workout.id;
            clickableWorkoutElement.querySelector(profilepageUtility.workoutElementTitle).textContent = workout.name;

            let localDate = new Date(workout.date);
            let table = clickableWorkoutElement.querySelector("table");
            let tableRows = table.querySelectorAll("tr");
            tableRows[0].querySelectorAll("td")[1].textContent = localDate.toLocaleDateString(); // Date
            tableRows[1].querySelectorAll("td")[1].textContent = localDate.toLocaleTimeString(); // Time
            tableRows[2].querySelectorAll("td")[1].textContent = workout.owner_username; //Owner
            tableRows[3].querySelectorAll("td")[1].textContent = workout.exercise_instances.length; // Exercises

            workoutListContainer.appendChild(clickableWorkoutElement);
        });
    }

    async #fetchShownUser() {
        const urlParams = new URLSearchParams(window.location.search);

        let id;
        if (!urlParams.has("id") || (id = urlParams.get("id")) === this.currentUser.id) {
            if (id === undefined) id = this.currentUser.id;
            this.#activateOwnerAccess(id, this.currentUser);
            return this.currentUser;
        }

        let userRequestUrl = this.apiRequester.buildRequestQuery(["users", id.toString()]);
        let fetchedUser = await this.apiRequester.apiRequest(METHOD.GET, userRequestUrl,
            profilepageUtility.ErrorMessages.FetchingOtherUserFailed);
        this.workoutHeading = fetchedUser.username + "'s workouts";

        return fetchedUser;
    }

    #retrieveUserProfile(user){
        let profileTemplate = document.querySelector(profilepageUtility.profileTemplateId.toElementId());
        let inputTemplate = profileTemplate.querySelector(profilepageUtility.profileEmailInputId.toElementId());

        const profileTemplateText = profileTemplate.querySelectorAll(profilepageUtility.profileTemplateTitle);
        profileTemplateText[0].textContent = user.username;
        inputTemplate.value = user.email;

        const profileDescription = profileTemplate.querySelector("p");
        profileDescription.textContent = profilepageUtility.profileDescriptionText;
    }

    #showProfileWorkouts(workouts, user){
        let workoutAnchors = document.querySelectorAll(profilepageUtility.workoutAnchor.toElementClassName());

        for (let j = 0; j < workouts.length; j++) {
            let workout = workouts[j];
            let workoutAnchor = workoutAnchors[j];

            if (workout.owner === user.url) {
                workoutAnchor.classList.remove("hide");
            } else {
                workoutAnchor.classList.add("hide");
            }
        }
    }

    #activateOwnerAccess(id, user){
        this.workoutHeading.textContent = "My workouts";

        this.deleteButton.classList.remove("hide");
        this.deleteButton.addEventListener("click", (async (id) => await this.#deleteUser(id)).bind(this, id));

        this.editButton.classList.remove("hide");
        this.editButton.addEventListener("click", this.#toggleEditMode.bind(this));

        this.cancelButton.addEventListener("click", this.#handleCancelButton.bind(this));
        this.okButton.addEventListener("click", (async (id) => await this.#editUser(id, user)).bind(this, id));
    }

    #toggleEditMode(isActive = true){
        setReadOnly(!isActive, profilepageUtility.profileFormId.toElementId());

        if (isActive){
            this.editButton.classList.add("hide");
            this.deleteButton.classList.add("hide");
            this.okButton.classList.remove("hide");
            this.cancelButton.classList.remove("hide");

            this.inputMail.className = "form-control text-center";
            this.oldMail = this.inputMail.value;
        }else {
            this.editButton.classList.remove("hide");
            this.deleteButton.classList.remove("hide");
            this.okButton.classList.add("hide");
            this.cancelButton.classList.add("hide");

            this.inputMail.className = "form-control-plaintext text-center";
            this.inputMail.value = this.oldMail;
        }
    }

    async #editUser(id, user) {
        let form = document.querySelector(profilepageUtility.profileFormId.toElementId());
        let formData = new FormData(form);
        user.email = formData.get("email");

        let updateUserRequestUrl = this.apiRequester.buildRequestQuery(["users", id.toString()]);
        await this.apiRequester.customApiRequest(METHOD.PATCH, updateUserRequestUrl, user,
            profilepageUtility.ErrorMessages.UpdatingUserProfileFailed);

        location.reload();
        this.#handleOkButton();
        document.body(profilepageUtility.profileUpdatedText);
    }

    async #deleteUser(id) {
        if (!confirm(profilepageUtility.profileDeletionConfirmText))
            return;

        let deleteUserRequestUrl = this.apiRequester.buildRequestQuery(["users", id.toString()]);
        await this.apiRequester.apiRequest(METHOD.DELETE, deleteUserRequestUrl,
            profilepageUtility.ErrorMessages.DeletingUserProfileFailed);

        this.#logoutUser();
    }

    #logoutUser(){
        deleteCookie("access");
        deleteCookie("refresh");
        deleteCookie("remember_me");
        sessionStorage.removeItem("username");
        window.location.replace(StringUtility.homeLocation);
    }

    #handleCancelButton() {
        this.#toggleEditMode(false);
    }

    #handleOkButton(){
        this.#toggleEditMode(false);
        oldEmail = null;
    }
}