class ApiRequester {
    constructor() {
        this.apiUrl = `${HOST}/api/`;
    }

    async customApiRequest(method, requestUrl, body, onErrorMessage = "error", onErrorPredicate = function () {return getCurrentUser() !== undefined;}){
        let response = await sendRequest(method, requestUrl, body);
        let data;
        await response.json()
            .then(responseData => data = responseData)
            .catch(error => console.log(error));

        if (!response.ok){
            console.log(response.status + " " + response.statusText);
            this.onError(onErrorMessage, onErrorPredicate, data);
            return null;
        }

        return data;
    }

    async apiRequest(method, requestUrl, onErrorMessage = "error", onErrorPredicate = function () {return getCurrentUser() !== undefined;}){
        return await this.customApiRequest(method, requestUrl, undefined, onErrorMessage, onErrorPredicate);
    }

    buildRequestQuery(params = [], queryArgs = {}){
        let requestUrl = this.apiUrl;

        if (params.length > 0)
            requestUrl += this.#buildUrlParamsString(params);

        if (Object.keys(queryArgs).length > 0)
            requestUrl += this.#buildQueryArgsString(queryArgs);

        return requestUrl;
    }

    #buildUrlParamsString(params){
        let paramString = "";
        params.forEach(param => paramString = paramString.concat(param.toString()).concat("/"));

        return paramString;
    }

    #buildQueryArgsString(queryArgs){
        let queryString = "?";
        let counter = 0;
        for (let property in queryArgs){
            if (!queryArgs.hasOwnProperty(property)) continue;

            if (counter > 0) queryString += "&";
            counter++;
            queryString += property + "=" + queryArgs[property];
        }

        return queryString;
    }

    onError(errorMessage, onErrorPredicate, data){
        if (!onErrorPredicate) return;
        if (!data) return;
        let alert = createAlert(errorMessage, data);
        document.body.prepend(alert);
    }
}

const METHOD = {
    GET: "GET",
    DELETE: "DELETE",
    POST: "POST",
    PUT: "PUT",
    PATCH: "PATCH"
};

const OFFER_STATUS = {
    ACCEPTED: "a",
    PENDING: "p",
    DECLINED: "d"
};

const OFFER_CATEGORY = {
    SENT: "sent",
    RECEIVED: "received"
};