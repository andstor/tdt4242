class StringUtility{
    static defaultWorkoutImageLocation = `${HOST}/media/workouts/0/Man-Doing-Crunches-At-Home.jpg`;
    static homeLocation = "index.html";

    static workoutHref = "workout.html?id=";
    static profilePageHref = "profilepage.html?id=";

    static DashboardUtility = class {
        static defaultImageClass = "dummy-image";
        static offerAmountElementClass = "offer-amount";

        static containerElementId = "dashboard-element";
        static welcomeTextId = "welcome-text";
        static introductionTextId = "introduction-text";

        static latestWorkoutLocationClass = "new-workout-text";
        static personalWorkoutLocationClass = "personal-workout-text";
        static latestPersonalWorkoutLocationClass = "latest-personal-workout-text";

        static latestWorkoutImagesName = "new-workout-img";
        static personalWorkoutImagesName = "personal-workout-img";
        static latestPersonalWorkoutImagesName = "latest-personal-workout-img";

        static previewImageClass = "preview-image";

        static ErrorMessages = class {
            static FetchingWorkoutsFailed = "Could not retrieve workout data for dashboard!";
            static FetchingOfferDataFailed = "Failed to load offer data for dashboard!";
        }
    };

    static UsersUtility = class {
        static usersContainerId = "div-content";
        static userTemplateId = "template-user";
        static userAnchorTitle = "h5";

        static ErrorMessages = class {
            static FetchingUsersFailed = "Could not retrieve user types!";
        }
    };

    static ProfilePageUtility = class {
        static deleteButtonId = "btn-delete-user";
        static editButtonId = "btn-edit-user";
        static cancelEditButtonId = "btn-cancel-user-edit";
        static confirmEditButtonId = "btn-ok-user-edit";

        static workoutHeadingId = "head";
        static workoutContainerId = "div-content";
        static workoutTemplateId = "template-user-workout";
        static workoutAnchor = "workout";

        static profileTemplateId = "profileinformation";
        static profileEmailInputId = "inputEmail";
        static profileDescriptionText = "You can see the associated workouts below and the profile information above.";
        static profileUpdatedText = "User information was updated";
        static profileDeletionConfirmText = "Are you sure you want to delete your account?";
        static profileFormId = "form-profile";

        static clickableTemplateWrapper = "a";
        static workoutElementTitle = "h5";
        static profileTemplateTitle = "h6";

        static ErrorMessages = class {
            static FetchingWorkoutsFailed = "Could not retrieve workout data for profile page!";
            static FetchingOtherUserFailed = "Could not retrieve other user's profile page!";
            static UpdatingUserProfileFailed = "Could not update users profile!";
            static DeletingUserProfileFailed = "Could not delete users profile!";
        }
    };
}

String.prototype.toElementClassName = function () {
    return "." + this.toString();
};

String.prototype.toElementId = function () {
    return "#" + this.toString();
};

String.prototype.toElementName = function () {
    return "[name='" + this.toString() + "']";
};

