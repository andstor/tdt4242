let apiRequester = new ApiRequester();
let usersUtility = StringUtility.UsersUtility;

async function retrieveUsers() {
    let requestUrl = apiRequester.buildRequestQuery(["users"]);
    let onErrorMessage = usersUtility.ErrorMessages.FetchingUsersFailed;
    let fetchedUserData = await apiRequester.apiRequest(METHOD.GET, requestUrl,onErrorMessage);

    let users = fetchedUserData.results;

    let usersListContainer = document.querySelector(usersUtility.usersContainerId.toElementId());
    let userTemplate = document.querySelector(usersUtility.userTemplateId.toElementId());

    users.forEach(user => {
        let userAnchor = userTemplate.content.firstElementChild.cloneNode(true);
        userAnchor.href = StringUtility.profilePageHref + user.id;

        const userAnchorTitle = userAnchor.querySelector(usersUtility.userAnchorTitle);
        userAnchorTitle.textContent = user.username;

        usersListContainer.appendChild(userAnchor);
    });
}

window.addEventListener("DOMContentLoaded", async () => retrieveUsers());