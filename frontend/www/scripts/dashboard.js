let dashboardUtility = StringUtility.DashboardUtility;

document.addEventListener("DOMContentLoaded", async () => {
    let currentUser = await getCurrentUser();
    let dashboard = new Dashboard(currentUser);

    dashboard.initDefaultImages();

    if (dashboard.toggleState())
        await dashboard.initializeData();
});

class Dashboard{
    constructor(currentUser) {
        this.currentUser = currentUser;
        this.apiRequester = new ApiRequester();

        this.boardElement = document.querySelector(dashboardUtility.containerElementId.toElementId());
        this.welcomeText = document.querySelector(dashboardUtility.welcomeTextId.toElementId());
        this.introductionText = document.querySelector(dashboardUtility.introductionTextId.toElementId());
    }

    initDefaultImages(){
        let dummyImages = document.querySelectorAll(dashboardUtility.defaultImageClass.toElementClassName());
        Array.from(dummyImages).forEach((dummyImage) => {dummyImage.src = StringUtility.defaultWorkoutImageLocation;});
    }

    toggleState(){
        let userLoggedIn = this.currentUser !== undefined;
        this.boardElement.style.visibility = userLoggedIn ? "visible" : "hidden";
        this.welcomeText.style.display = userLoggedIn ? "none" : "visible";
        this.introductionText.style.visibility = userLoggedIn ? "visible" : "none";

        return userLoggedIn;
    }

    async initializeData() {
        let fetchedOfferData = await this.#fetchOfferData();
        let fetchedWorkoutData = await this.#fetchWorkoutData();

        this.#initiateOfferData(fetchedOfferData);
        this.#initiateWorkoutData(fetchedWorkoutData);
    }

    async #fetchOfferData(){
        let queryArgs = {status: OFFER_STATUS.PENDING, category: OFFER_CATEGORY.RECEIVED};
        let requestUrl = this.apiRequester.buildRequestQuery(["offers"], queryArgs);
        let fetchedOfferData = await this.apiRequester.apiRequest(METHOD.GET, requestUrl,
            dashboardUtility.ErrorMessages.FetchingOfferDataFailed);

        return fetchedOfferData;
    }

    async #fetchWorkoutData(){
        let requestUrl = this.apiRequester.buildRequestQuery(["workouts"]);
        let fetchedWorkoutData = await this.apiRequester.apiRequest(METHOD.GET, requestUrl,
            dashboardUtility.ErrorMessages.FetchingWorkoutsFailed);

        return fetchedWorkoutData;
    }

    #initiateOfferData(fetchedOfferData){
        let offerAmountElement = document.querySelector(dashboardUtility.offerAmountElementClass.toElementClassName());
        offerAmountElement.textContent = fetchedOfferData.results.length.toString();
    }

    async #initiateWorkoutData(fetchedWorkoutData){
        let workouts = fetchedWorkoutData.results;

        let latestWorkouts = this.#insertLatestWorkouts(workouts);
        let personalWorkouts = this.#insertPersonalWorkouts(workouts);
        this.#insertLatestPersonalWorkouts(personalWorkouts.filter(value => latestWorkouts.includes(value)));
    }

    #insertLatestWorkouts(workouts){
        let sortedWorkouts = workouts.sort(function (firstDate, secondDate) {
            return firstDate.date - secondDate.date;
        });

        let latestWorkoutLocations = document.querySelectorAll(dashboardUtility.latestWorkoutLocationClass.toElementClassName());
        this.#extractWorkoutData(latestWorkoutLocations, dashboardUtility.latestWorkoutImagesName, sortedWorkouts);

        return sortedWorkouts;
    }

    #insertPersonalWorkouts(workouts){
        let personalWorkouts = Array.from(workouts).filter((workout) => workout.owner_username === this.currentUser.username);

        let personalWorkoutLocations = document.querySelectorAll(dashboardUtility.personalWorkoutLocationClass.toElementClassName());
        this.#extractWorkoutData(personalWorkoutLocations, dashboardUtility.personalWorkoutImagesName, personalWorkouts);

        return personalWorkouts;
    }

    #insertLatestPersonalWorkouts(latestPersonalWorkouts){

        let latestPersonalWorkoutLocation = document.querySelectorAll(dashboardUtility.latestPersonalWorkoutLocationClass.toElementClassName());
        this.#extractWorkoutData(latestPersonalWorkoutLocation, dashboardUtility.latestPersonalWorkoutImagesName, latestPersonalWorkouts);
    }

    async #extractWorkoutData(locatedElements, locatedImageName, workoutData){
        for(let i = 0; i < locatedElements.length && i < workoutData.length; i++){
            let currentWorkout = workoutData[i];
            locatedElements[i].textContent = currentWorkout.name;

            let previewImageResponse = await this.#fetchWorkoutPreviewFile(currentWorkout);

            let previewImageLocation = document.querySelector(
                locatedImageName.toElementClassName() + "column-".toElementClassName() + (i+1));
            let previewImage = this.#insertValidWorkoutImage(previewImageResponse, previewImageLocation);
            this.#insertClickableWrapperElement(previewImage, previewImageLocation,
                StringUtility.workoutHref + currentWorkout.id);
        }
    }

    async #fetchWorkoutPreviewFile(workoutObject){
        let workoutFileReferences = workoutObject.files;
        if (workoutFileReferences.length <= 0) return null;

        let workoutFileUrls = Array.from(workoutFileReferences)
            .filter((workoutFileUrl) =>
                workoutFileUrl.file.endsWith(".jpg") ||
                workoutFileUrl.file.endsWith(".png"));

        return loadImage(workoutFileUrls[0].file);
    }

    #insertValidWorkoutImage(previewImageResponse, previewImageLocation){
        let foundDefaultImage = previewImageLocation.querySelector(
            dashboardUtility.defaultImageClass.toElementClassName());

        if (previewImageResponse == null) return foundDefaultImage;
        else foundDefaultImage?.remove();

        let previewImage = new Image();
        previewImage.crossOrigin = "";

        previewImage.src = window.URL.createObjectURL(previewImageResponse);
        previewImage.className = dashboardUtility.previewImageClass;

        previewImageLocation.append(previewImage);

        return previewImage;
    }

    #insertClickableWrapperElement(previewImageElement = null, previewImageLocationElement = null, hrefUrl = ""){
        if (previewImageLocationElement == null)
            previewImageLocationElement = previewImageElement.parentNode;

        let clickableWrapperElement = document.createElement("a");

        clickableWrapperElement.href = hrefUrl.length === 0 ? StringUtility.workoutHref + "0" : hrefUrl;
        clickableWrapperElement.appendChild(previewImageElement);

        previewImageLocationElement.appendChild(clickableWrapperElement);
    }
}